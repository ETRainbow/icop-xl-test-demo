
## 关于 icop-xl-test-demo 项目

1.这个只是一个测试项目，其主要测试限流icop-xl-spring-boot-starter起步依赖中限流的作用 <br/>
2.待测试项目地址：https://gitlab.com/ETRainbow/icop-xl-spring-boot-starter.git <br/>
3.使用依赖示例：（使用依赖时请注意版本号） <br/>
`<dependency> 
    <groupId>com.icop.xl</groupId> 
    <artifactId>icop-xl-spring-boot-starter</artifactId> 
    <version>0.0.1-SNAPSHOT</version>
</dependency>`