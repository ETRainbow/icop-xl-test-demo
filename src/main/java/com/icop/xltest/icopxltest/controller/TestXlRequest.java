package com.icop.xltest.icopxltest.controller;

import com.icop.xl.aspect.XlHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: liukj
 * @date: 2020/3/27
 * @description：
 */
@Controller
@Slf4j
@RequestMapping(value = "/xltest")
public class TestXlRequest {

    @PostMapping("/testLimitHandler.json")
    @XlHandler(limitSwitch = "ON")
    @ResponseBody
    public String testXlHandler(HttpServletRequest request){

        log.debug("请求进入");
        return "limitHandler处理返回";
    }

    @PostMapping("/testLimitHandler2.json")
    @XlHandler(limitSwitch = "ON",limitType = "DYNAMIC",limitNum = 3,timeOut =1000,limitPeriod = 360000)
    @ResponseBody
    public String testXlHandler2(HttpServletRequest request){

        log.debug("请求进入");
        return "limitHandler处理返回";
    }

    @PostMapping("/testLimitHandler3.json")
    @XlHandler(limitSwitch = "ON",limitType = "DYNAMIC",limitNum = 5,timeOut =0,limitPeriod =10000)
    @ResponseBody
    public String testXlHandler3(HttpServletRequest request){

        log.debug("请求进入");
        return "limitHandler处理返回";
    }

}
