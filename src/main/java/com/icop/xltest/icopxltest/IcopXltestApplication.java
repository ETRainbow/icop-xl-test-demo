package com.icop.xltest.icopxltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IcopXltestApplication {

    public static void main(String[] args) {
        SpringApplication.run(IcopXltestApplication.class, args);
    }

}
